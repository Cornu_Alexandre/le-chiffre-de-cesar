import unittest
from chiffre_cesar import comptage_caractere, fonct_cryptage, fonct_decryptage

class TestChiffreCesar(unittest.TestCase):

    def test_comptage_caractere(self):
        self.assertEqual(comptage_caractere("Bonjour Toi!"),12)
        self.assertEqual(comptage_caractere(""),0)
    
    def test_cryptage(self):
        self.assertEqual(fonct_cryptage("bon"),"jwv")

    def test_decryptage(self):
        self.assertEqual(fonct_decryptage("jwv"),"bon")






