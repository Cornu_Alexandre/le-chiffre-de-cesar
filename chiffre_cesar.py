
from tkinter import *
from tkinter.messagebox import *

def comptage_caractere(string):

    return len(string)

def fonct_cryptage(string):
    conversion_cryptage="" 
    for char in string:
        conversion_cryptage = conversion_cryptage + chr(ord(char)+int(decalage_content.get()))
         
    return conversion_cryptage


def fonct_decryptage(string):
    conversion_decryptage=""
    for char in string:
        conversion_decryptage = conversion_decryptage + chr(ord(char)-int(decalage_content.get()))
        
    return conversion_decryptage


def obtenir_texte_content():
 
    return texte_content.get("0.0", END).strip()

def check_texte_content():
    
    string = obtenir_texte_content()
    if len(string) ==0:
        showwarning(title="Erreur", message="Inserez un teste ici svp")
        return False
    return True
    
    
def cryptage_callback():

    if check_texte_content():
        string = obtenir_texte_content()
        showinfo(title="Info sur votre texte crypter",
                    message="Votre texte crypté vaut : %s" % fonct_cryptage(string))

def decryptage_callback():
    if check_texte_content():
        string = obtenir_texte_content()
        showinfo(title="Info sur votre texte decrypter",
                    message="Votre texte decrypté vaut: %s" % fonct_decryptage(string))

color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"


frame = Tk()
frame.title("Site de Cryptage/Décryptage")
frame.minsize(500, 450)

frame.rowconfigure(0, weight=1)
frame.rowconfigure(1, weight=1)
frame.rowconfigure(2, weight=1)
frame.rowconfigure(3, weight=1)

frame.columnconfigure(0, weight=1)
frame.columnconfigure(1, weight=1)
frame.columnconfigure(2, weight=1)

texte_content = Text(frame, wrap='word')
texte_content.grid(row=0, columnspan=1)

decalage_content = StringVar() 
entry = Entry(frame, textvariable=decalage_content, width=30)
entry.grid(row=1, columnspan=1)

bouton_crypter = Button(frame, text="Crypter", command=cryptage_callback)
bouton_crypter.grid(row=2, columnspan=2)

bouton_decrypter = Button(frame, text="Décrypter", command=decryptage_callback)
bouton_decrypter.grid(row=3, columnspan=2)

frame.mainloop()
